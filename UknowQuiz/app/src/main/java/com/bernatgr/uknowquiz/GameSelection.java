package com.bernatgr.uknowquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GameSelection extends AppCompatActivity {

    private Button btnTimed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_selection);
        btnTimed = (Button) findViewById(R.id.btnTimed);

        btnTimed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(GameSelection.this, SelectTheme.class);
                startActivity(i);
                finish();
            }
        });
    }
}

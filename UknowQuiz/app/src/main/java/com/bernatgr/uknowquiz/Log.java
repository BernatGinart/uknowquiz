package com.bernatgr.uknowquiz;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

public class Log extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private GoogleSignInOptions mGso;
    private GoogleApiClient mGoogleApiClient;

    private Button btnInicioSesion;
    private Button btnRegist;
    private EditText etEmail;
    private EditText etPassword;
    private SignInButton btnsignInGoolge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnInicioSesion = (Button) findViewById(R.id.btnInicioSesion);
        btnRegist = (Button) findViewById(R.id.bntRegist);
        btnsignInGoolge = (SignInButton) findViewById(R.id.google_signIn);


        btnInicioSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn(etEmail.getText().toString(), etPassword.getText().toString());
            }
        });

        btnRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Log.this, Regist.class);
                startActivity(intent);
            }
        });

        btnsignInGoolge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                googleSingIn();

            }
        });


        mAuth = FirebaseAuth.getInstance();

        mGso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, mGso)
                .build();

    }


    public void googleSingIn() {

        if (mAuth.getCurrentUser() == null){
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient);
        }

        Intent singIn = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(singIn, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            if (result.isSuccess()) {

                GoogleSignInAccount account = result.getSignInAccount();
                handleSingIn(account);

            }else{

            }
        }

    }

    private void handleSingIn(GoogleSignInAccount account) {

            AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {

                            } else {
                                Intent i = new Intent(Log.this, Splash.class);
                                startActivity(i);
                                finish();
                            }
                            // ...
                        }
                    });

    }

    public void logIn(String email, String password) {
        try {

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                Toast.makeText(Log.this, "Usuario/Contraseña incorrecto",
                                        Toast.LENGTH_SHORT).show();
                            } else {

                                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                                Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient);

                                Intent i = new Intent(Log.this, Splash.class);
                                startActivity(i);
                                finish();
                            }
                            // ...
                        }
                    });
        } catch (Exception e) {
            Toast.makeText(this, "Falta el usuario o la contraseña", Toast.LENGTH_SHORT).show();
        }
    }

}



package com.bernatgr.uknowquiz;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import java.io.IOException;


public class Regist extends AppCompatActivity {

    private Button btnRegistrarse;

    private EditText etNewEmail;
    private EditText etNewPassword;
    private ImageButton ibFotoperfil;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);

        etNewEmail = (EditText) findViewById(R.id.etNewEmail);
        etNewPassword = (EditText) findViewById(R.id.etNewPassword);


        btnRegistrarse = (Button) findViewById(R.id.btnRegistrarse);
        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewUser(etNewEmail.getText().toString(), etNewPassword.getText().toString());
            }
        });

        ibFotoperfil  = (ImageButton) findViewById(R.id.ibFotoPerfil);
        ibFotoperfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateDialog(savedInstanceState);
            }
        });
    }


    public void NewUser(String email, String password){

        if (password.length()>=6) {
            try{
                final FirebaseAuth mAuth = FirebaseAuth.getInstance();
                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(Regist.this, "Error al crear usuario",
                                                Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(Regist.this, "Usuario creado", Toast.LENGTH_SHORT).show();
                                        Intent i = new Intent(Regist.this, Splash.class);
                                        startActivity(i);
                                        finish();
                                    }
                                }
                            });
            }catch (Exception e){
                Toast.makeText(this, "Falta el usuario o la contraseña", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "La contraseña debe tener mínimo 6 letras", Toast.LENGTH_SHORT).show();
        }
    }

    public Dialog CreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final CharSequence[] Opciones = new CharSequence[2];
        Opciones[0]="Galeria";
        Opciones[1]="Cámara";

        builder.setTitle("")
                .setItems(Opciones, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                       if (which == 1){
                            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(takePicture, 0);
                       }else{
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto , 1);
                       }
                    }
                });
        builder.show();
        return builder.create();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            //Camera
            case 0:

                if(resultCode == RESULT_OK){

                    try {
                        Bitmap imageSelected = (Bitmap) imageReturnedIntent.getExtras().get("imageReturnedIntent");

                        if (imageSelected.getWidth() > 4096 || imageSelected.getHeight() > 4096){
                            ibFotoperfil.setImageBitmap(scaleImage(imageSelected));
                        }else ibFotoperfil.setImageBitmap(imageSelected);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            //Galery
            case 1:
                if(resultCode == RESULT_OK){

                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        Bitmap imageSelected = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);

                        if (imageSelected.getWidth() > 4096 || imageSelected.getHeight() > 4096){
                            ibFotoperfil.setImageBitmap(scaleImage(imageSelected));
                        }else ibFotoperfil.setImageBitmap(imageSelected);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    public Bitmap scaleImage(Bitmap bitmap){

        float scaledWith = (100.0f) / bitmap.getWidth();
        float scaledHight = (100.0f) / bitmap.getHeight();

        Matrix matrix = new Matrix();
        matrix.postScale(scaledWith, scaledHight);

        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);

        return resizedBitmap;
    }

}

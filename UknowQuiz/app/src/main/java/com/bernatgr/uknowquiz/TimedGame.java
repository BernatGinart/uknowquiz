package com.bernatgr.uknowquiz;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


public class TimedGame extends AppCompatActivity{

    private TextView tvPregunta;
    private Button btnAnsw1;
    private Button btnAnsw2;
    private Button btnAnsw3;
    private Button btnAnsw4;

    private List<String> questions = new ArrayList<>();
    private List<HashMap<String, String>> answers = new ArrayList<>();

    private Random randomIndex = new Random();

    private int position;

    private List<String> keyList;
    private Handler mHandler = new Handler();
    private Runnable mRunnable;
    private ProgressBar TimerBar;
    private CountDownTimer mCountDown;
    private int progress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timed_game);

        FillQuestionsAnswersList();

        tvPregunta = (TextView) findViewById(R.id.tvpregunta);
        btnAnsw1 = (Button) findViewById(R.id.btnAnsw1);
        btnAnsw2 = (Button) findViewById(R.id.btnAnsw2);
        btnAnsw3 = (Button) findViewById(R.id.btnAnsw3);
        btnAnsw4 = (Button) findViewById(R.id.btnAnsw4);
        TimerBar = (ProgressBar) findViewById(R.id.Timer);

        questionMaker();

        btnAnsw1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nextQuestion(btnAnsw1);
            }

        });
        btnAnsw2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nextQuestion(btnAnsw2);
            }
        });
        btnAnsw3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nextQuestion(btnAnsw3);
            }
        });
        btnAnsw4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nextQuestion(btnAnsw4);
            }
        });

        TimerBar.setProgress(progress);

        mCountDown = new CountDownTimer(60000, 800) {
            @Override
            public void onTick(long millisUntilFinished) {
                progress++;
                TimerBar.setProgress(progress);
            }

            @Override
            public void onFinish() {
                finish();
                Toast.makeText(TimedGame.this, "Time Out !!", Toast.LENGTH_SHORT).show();
            }
        };
        mCountDown.start();

    }

    @Override
    public void finish() {
        super.finish();
        mCountDown.cancel();
    }

    public void FillQuestionsAnswersList() {

        for (int i = 1; i <= Splash.questionsMap.get(getIntent().getStringExtra("GAME_THEME")).size(); i++) {
            String keyQuestions = "Q" + i;
            String keyAnswers = "AQ" + i;
            questions.add(Splash.questionsMap.get(getIntent().getStringExtra("GAME_THEME")).get(keyQuestions));
            answers.add(Splash.answersMap.get(getIntent().getStringExtra("GAME_THEME")).get(keyAnswers));
        }
    }

    public void questionMaker() {


        if (questions.isEmpty()) {

            tvPregunta.setText("");
            Toast.makeText(this, "No hay mas preguntas...", Toast.LENGTH_SHORT).show();
            mHandler.removeCallbacks(mRunnable);
            finish();

        } else {

            position = randomIndex.nextInt(questions.size());

            keyList = new ArrayList<>(answers.get(position).keySet());

            tvPregunta.setText(questions.get(position));

            btnAnsw1.setText(answers.get(position).get(answerKey()));

            btnAnsw2.setText(answers.get(position).get(answerKey()));

            btnAnsw3.setText(answers.get(position).get(answerKey()));

            btnAnsw4.setText(answers.get(position).get(answerKey()));

        }
    }

    public String answerKey() {

        int indx = randomIndex.nextInt(keyList.size());

        if (keyList.get(indx).equals("correcta")){
            keyList.remove(indx);
            indx = randomIndex.nextInt(keyList.size());
        }

        String s = keyList.get(indx);
        keyList.remove(indx);
        return s;

    }

    public boolean correcta(String text) {

        if (answers.get(position).get("correcta").equals(text)) {

            questions.remove(position);
            answers.remove(position);

            return true;
        }

        questions.remove(position);
        answers.remove(position);

        return false;

    }

    public void nextQuestion(final Button button) {

        if (correcta(button.getText().toString())) {
            button.setBackgroundColor(Color.GREEN);
        } else {
            button.setBackgroundColor(Color.RED);
        }

        btnAnsw1.setClickable(false);
        btnAnsw2.setClickable(false);
        btnAnsw3.setClickable(false);
        btnAnsw4.setClickable(false);

        mHandler.postDelayed(mRunnable = new Runnable() {
            @Override
            public void run() {
                questionMaker();
                button.setBackgroundColor(Color.TRANSPARENT);

                btnAnsw1.setClickable(true);
                btnAnsw2.setClickable(true);
                btnAnsw3.setClickable(true);
                btnAnsw4.setClickable(true);
            }
        }, 250);
    }

}





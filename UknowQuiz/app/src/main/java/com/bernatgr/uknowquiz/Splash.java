package com.bernatgr.uknowquiz;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.HashMap;


public class Splash extends AppCompatActivity {

    protected static HashMap<String, HashMap<String, String>> questionsMap = new HashMap<>();
    protected static HashMap<String, HashMap<String, HashMap<String, String>>> answersMap = new HashMap<>();

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private static boolean called = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (!called){
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            called = true;
        }

        final DatabaseReference myRootRef = FirebaseDatabase.getInstance().getReference();
        myRootRef.keepSynced(true);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                    // User is signed in

                    myRootRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            questionsMap = (HashMap)dataSnapshot.child("Questions").getValue();
                            answersMap = (HashMap) dataSnapshot.child("Answers").getValue();

                            Intent home = new Intent(Splash.this, StartScreen.class);
                            startActivity(home);
                            finish();

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                } else {

                    // User is signed out

                    Intent i = new Intent(Splash.this, Log.class);
                    startActivity(i);
                    finish();
                }
                // ...
            }
        };


    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

}

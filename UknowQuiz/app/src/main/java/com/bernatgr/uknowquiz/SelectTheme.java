package com.bernatgr.uknowquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class SelectTheme extends AppCompatActivity {

    private Button btnTheme1;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_theme);

        btnTheme1 = (Button) findViewById(R.id.btnTheme1);
        btnTheme1.setText("Tecnología");
        btnTheme1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                themeSelected(btnTheme1.getText().toString());
            }
        });

    }

    public void themeSelected(String theme){
        Intent i = new Intent(SelectTheme.this, TimedGame.class);
        i.putExtra("GAME_THEME", theme);
        startActivity(i);
        finish();
    }
}
